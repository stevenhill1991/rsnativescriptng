import {Injectable}     from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import {Observable} from "rxjs/Rx";
import {SkillsHelper} from "../util/skillsHelper";

@Injectable()
export class HighscoresService {

  public HIGHSCORES_STANDARD_ACCOUNT_URL = "http://services.runescape.com/m=hiscore_oldschool/index_lite.ws?player=";
  private HIGHSCORES_IRONMAN_ACCOUNT_URL = "http://services.runescape.com/m=hiscore_oldschool_ironman/index_lite.ws?player=";
  private HIGHSCORES_ULTIMATE_ACCOUNT_URL = "http://services.runescape.com/m=hiscore_oldschool_ultimate/index_lite.ws?player=";

  constructor (private _http: Http) {} 

  getHighscoresData (userName: string, url : string) {    
    console.log("get highscoresData", url+userName);
    return this._http.get(url + userName).map(res => res.text()).catch(this.handleErrors);
  }

 transformHighscoresData(highscoresData: any){
   let userData = highscoresData.split(/\n/);
   var skills = SkillsHelper.getSkillsArray();
   return skills.map(function(skill){
     var skillData = userData.shift().split(',');
     return {
       name: skill,
       rank: skillData[0],
       level: skillData[1],
       xp: skillData[2]
     } 
    });
   }

   getPlayerData (userName){
     return Observable.forkJoin(
       this.getHighscoresData(userName, this.HIGHSCORES_STANDARD_ACCOUNT_URL),
       this.getHighscoresData(userName, this.HIGHSCORES_IRONMAN_ACCOUNT_URL),
       this.getHighscoresData(userName, this.HIGHSCORES_ULTIMATE_ACCOUNT_URL)
     );
   }

  handleErrors(error: Response) {
    console.log(JSON.stringify(error.json()));
    return Observable.throw(error);
  }

}