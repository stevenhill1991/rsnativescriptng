export module SkillActionsCalculator {

    var XP_ARRAY = [0, 0, 83, 174, 276, 388, 512, 650, 801, 969, 1154, 1358, 1584, 1833, 2107, 2411, 2746, 3115, 3523, 3973, 4470, 5018, 5624, 6291, 7028, 7842, 8740, 9730, 10824, 12031, 13363, 14833, 16456, 18247, 20224, 22406, 24815, 27473, 30408, 33648, 37224, 41171, 45529, 50339, 55649, 61512, 67983, 75127, 83014, 91721, 101333, 111945, 123660, 136594, 150872, 166636, 184040, 203254, 224466, 247886, 273742, 302288, 333804, 368599, 407015, 449428, 496254, 547953, 605032, 668051, 737627, 814445, 899257, 992895, 1096278, 1210421, 1336443, 1475581, 1629200, 1798808, 1986068, 2192818, 2421087, 2673114, 2951373, 3258594, 3597792, 3972294, 4385776, 4842295, 5346332, 5902831, 6517253, 7195629, 7944614, 8771558, 9684577, 10692629, 11805606, 13034431];

    function getExperienceForLevel(level) {
        return XP_ARRAY[level];
    }

    var SKILL_ACTIONS_STORE: any = {
            Firemaking: [{
                name: "Logs",
                level: 1,
                xp: getExperienceForLevel(1),
                xpPerAction: 40
            }, {
                name: "Oak logs",
                level: 15,
                xp: getExperienceForLevel(15),
                xpPerAction: 60
            }, {
                name: "Willow logs",
                level: 30,
                xp: getExperienceForLevel(30),
                xpPerAction: 90
            }, {
                name: "Maple logs",
                level: 45,
                xp: getExperienceForLevel(45),
                xpPerAction: 135
            }, {
                name: "Yew logs",
                level: 60,
                xp: getExperienceForLevel(60),
                xpPerAction: 202.5
            }, {
                name: "Magic logs",
                level: 75,
                xp: getExperienceForLevel(75),
                xpPerAction: 303.8
            }, {
                name: "Redwood logs",
                level: 90,
                xp: getExperienceForLevel(90),
                xpPerAction: 350
            }],
            Woodcutting: [{
                name: "Logs",
                level: 1,
                xp: getExperienceForLevel(1),
                xpPerAction: 25
            }, {
                name: "Oak logs",
                level: 15,
                xp: getExperienceForLevel(15),
                xpPerAction: 37.5
            }, {
                name: "Willow logs",
                level: 30,
                xp: getExperienceForLevel(30),
                xpPerAction: 67.5
            }, {
                name: "Teak logs",
                level: 35,
                xp: getExperienceForLevel(35),
                xpPerAction: 85
            }]
    }

    function calculateActions(currentXp, goalXp, itemsToLevelWith): Array<Object> | Object {
        if(itemsToLevelWith == null){
            return {
                error: "No actions for selected skill"
            }
        }
        if (currentXp > goalXp) {
            return {
                error: "Already at goal!"
            }
        }
        var actions = [];
        itemsToLevelWith.forEach(function (item, i) {
            var highLevelAction = itemsToLevelWith[i + 1] ? currentXp >= itemsToLevelWith[i + 1].xp : currentXp >= goalXp;
            if (highLevelAction) {
                return actions.push(0);
            }
            var neededXp;
            if (itemsToLevelWith[i + 1]) {               
                if (goalXp < itemsToLevelWith[i + 1].xp) {
                    neededXp = goalXp - currentXp;
                } else {
                    neededXp = itemsToLevelWith[i + 1].xp - currentXp;
                }
            } else {
                neededXp = goalXp - currentXp;
            }
            
            var neededActions = Math.ceil(neededXp / item.xpPerAction);
            actions.push({
                itemName: item.name,
                itemLevel: item.level,
                xpPerAction: item.xpPerAction,
                neededActions: neededActions
            });

            currentXp += (neededActions * item.xpPerAction);
        });
        return actions;
    }

    export function getSkillActions(currentXp, goalXp, skillName) {      
        return calculateActions(currentXp, goalXp, SKILL_ACTIONS_STORE[skillName]);
    }
};