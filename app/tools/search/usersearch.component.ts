import {Component} from "@angular/core";
import {UserSearchBarComponent} from "./usersearch-bar/usersearch-bar.component";
import {UserSearchListViewComponent} from "./usersearch-listview/usersearch-listview.component";
import {HTTP_PROVIDERS} from '@angular/http';

@Component({
    selector: "user-search",
    templateUrl: "./tools/search/usersearch.component.html",
    directives: [UserSearchBarComponent, UserSearchListViewComponent],
    providers: [HTTP_PROVIDERS]
})
export class UserSearchComponent {
    public userData: Array<Object>

    onUserData($event: any){
        console.log("main component has user data", $event);
        this.userData = $event;
    }
}
