import {Component, EventEmitter, Output} from "@angular/core";
import {HighscoresService} from '../../../services/core/highscores/highscores.service';

@Component({
    selector: "user-search-bar",
    templateUrl: "./tools/search/usersearch-bar/usersearch-bar.component.html",
    providers: [HighscoresService]
})
export class UserSearchBarComponent {
    searchErrorMessage: string = "";
    userName: string = "";
    searching: Boolean = false;
    @Output('UserData') userData = new EventEmitter();

    
    constructor(private _highscoresService: HighscoresService){
   
    }

   getHighscoresData(){

    this.searchErrorMessage="";

    if(!this.userName){
      this.searchErrorMessage="Please enter a username";
      return;
    }
    
    this.searching = true;    
    this._highscoresService.getHighscoresData(this.userName, this._highscoresService.HIGHSCORES_STANDARD_ACCOUNT_URL)   
        .subscribe(
                (data) =>{                                   
                    this.searching = false;
                    var playerData = this._highscoresService.transformHighscoresData(data);                             
                    this.userData.emit(playerData);
                },
                (err) => {
                    this.searching = false;
                    console.log('error getting highscores data', err)
                }
            );  
            
    }

}
