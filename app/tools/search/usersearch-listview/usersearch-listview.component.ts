import {Component, Input} from "@angular/core";
import {SkillActionsCalculator} from "../../../services/core/skills/actions";

@Component({
    selector: "user-search-listview",
    templateUrl: "./tools/search/usersearch-listview/usersearch-listview.component.html",
})

export class UserSearchListViewComponent {    
    @Input('UserData') userData: any;
    public isActiveTable;
    public gettingSkillData = false;
    public skillData;

     public onItemTap(args) {
        console.log("------------------------ ItemTapped: " + args.index);
        console.log(this.userData[args.index].name);
    }
    public onHeaderClicked(skillName: string, skillXp: string){        
        
        if( this.isActiveTable === skillName){
            this.isActiveTable = "";
            this.gettingSkillData = false;
            return;
        }

        this.isActiveTable = skillName;
        this.gettingSkillData = true;
        console.log("getting skill actions using", parseInt(skillXp, 10), 200000000, skillName);
        this.skillData = SkillActionsCalculator.getSkillActions(parseInt(skillXp, 10), 200000000, skillName);
        this.gettingSkillData = false;
    }
}
