// this import should be first in order to load some required settings (like globals and reflect-metadata)
import {nativeScriptBootstrap} from "nativescript-angular/application";
import {UserSearchComponent} from "./tools/search/usersearch.component";
// HACK - patch dom adapter to enable http_providers to not crash
import {Parse5DomAdapter} from '@angular/platform-server/src/parse5_adapter';
(<any>Parse5DomAdapter).prototype.getCookie = function (name) { return null; };

nativeScriptBootstrap(UserSearchComponent);